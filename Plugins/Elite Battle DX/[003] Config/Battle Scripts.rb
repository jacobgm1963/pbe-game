#===============================================================================
#  Elite Battle: DX
#    by Luka S.J.
# ----------------
#  Battle Scripts
#===============================================================================
module BattleScripts
  # example scripted battle for PIDGEY
  # you can define other scripted battles in here or make your own section
  # with the BattleScripts module for better organization as to not clutter the
  # main EBDX cofiguration script (or keep it here if you want to, your call)
  PIDGEY = {
    "turnStart0" => {
      :text => [
        "Wow! This here Pidgey is among the top percentage of Pidgey.",
        "I have never seen such a strong Pidgey!",
        "Btw, this feature works even during wild battles.",
        "Pretty exciting, right?"
      ],
      :file => "trainer024"
    }
  }
  # to call this battle script run the script from an event jusst before the
  # desired battle:
  #    EliteBattle.set(:nextBattleScript, :PIDGEY)
  #-----------------------------------------------------------------------------
  # example scripted battle for BROCK
  # this one is added to the EBDX trainers PBS as a BattleScript parameter
  # for the specific battle of LEADER_Brock "Brock" trainer
  BROCK = {
    "turnStart0" => proc do
      pname = @battlers[1].name
      tname = @battle.opponent[0].name
      # begin code block for the first turn
      @scene.pbTrainerSpeak(["Time to set this battle into motion!",
                             "Let's see if you'll be able to handle my #{pname} after I give him this this!"
                           ])
      # play common animation for Item use args(anim_name, scene, index)
      @scene.pbDisplay("#{tname} tossed an item to the #{pname} ...")
      EliteBattle.playCommonAnimation(:USEITEM, @scene, 1)
      # play aura flare
      @scene.pbDisplay("Immense energy is swelling up in the #{pname}")
      EliteBattle.playCommonAnimation(:AURAFLARE, @scene, 1)
      @vector.reset # AURAFLARE doesn't reset the vector by default
      @scene.wait(16, true) # set true to anchor the sprites to vector
      # raise battler Attack sharply (doesn't display text)
      @battlers[1].pbRaiseStatStageBasic(:ATTACK, 2)
      # show trainer speaking additional text
      @scene.pbTrainerSpeak("My #{pname} will not falter!")
      # show generic text
      @scene.pbDisplay("The battle is getting intense! You see the lights and stage around you shift.")
      # change Battle Environment (with white fade)
      pbBGMPlay("Battle Elite")
      @sprites["battlebg"].reconfigure(:STAGE, Color.white)
    end,
    "damageOpp" => "Woah! A powerful move!",
    "damageOpp2" => "Another powerful move ...",
    "lastOpp" => "This is it! Let's make it count!",
    "lowHPOpp" => "Hang in there!",
    "attack" => "Whatever you throw at me, my team can take it!",
    "attackOpp" => "How about you try this one on for size!",
    "fainted" => "That's how we do it in this gym!",
    "faintedOpp" => "Arghh. You did well my friend...",
    "loss" => "You can come back and challenge me any time you want."
  }
  #-----------------------------------------------------------------------------
  # example Dialga fight
  MAVROS = {
    "turnStart0" => proc do
      # hide databoxes
      @scene.pbHideAllDataboxes
      # show flavor text
      @scene.pbDisplay("The very air around you turns to darkness as Mavros, the Dark Beast, approaches...")
      # play common animation
      EliteBattle.playCommonAnimation(:ROAR, @scene, 1)
      @scene.pbDisplay("Mavros roars...")
      # change the battle environment (use animation to transition)
      @sprites["battlebg"].reconfigure(:DIMENSION, :DISTORTION)
      @scene.pbDisplay("And the battlefield clears.")
      @scene.pbDisplay("Mavros acknowledges your strength, and allows you to battle it!")
      # show databoxes
      @scene.pbShowAllDataboxes
    end
  }
  #-----------------------------------------------------------------------------
  # final battle with Donari
  DONARIFINAL = {
    "turnStart0" => proc do
	  pname = @battlers[1].name
      tname = @battle.opponent[0].name
      # hide databoxes
      @scene.pbHideAllDataboxes
      # show flavor text
      @scene.pbTrainerSpeak(["I warned you so many times.",
                             "I saw so much potential in you, and yet here you are - wasting it.",
							 "Look around you! All this could have been avoided!",
							 "This is what I meant. This world is unchanging, uncaring.",
							 "It's up to people like us to fix that.",
							 "There are two sides to this.",
							 "I'm sorry...",
							 "But you picked the wrong one.",
							 "This is the end for you."
                           ])
      # play common animation
      EliteBattle.playCommonAnimation(:VOIDFLARE, @scene, 1)
	  @vector.reset # AURAFLARE doesn't reset the vector by default
      @scene.wait(16, true) # set true to anchor the sprites to vector
      # change the battle environment (use animation to transition)
	  pbBGMPlay("BGM_FINALBATTLE2.ogg")
      # show databoxes
      @scene.pbShowAllDataboxes
    end,
	"halfHPOpp" => proc do
      @scene.pbTrainerSpeak(["No.",
                             "Accept it. You don't stand a chance.",
							 "And now... you will be destroyed!"
                           ])
	  EliteBattle.playCommonAnimation(:FORMCHANGE, @scene, 1)
	  @battlers[1].pbRaiseStatStageBasic(:DEFENSE, 1)
	  @battlers[1].pbRaiseStatStageBasic(:SPECIAL_DEFENSE, 1)
	  @battlers[1].pbRecoverHP(400)
	  pbBGMPlay("BGM_FINALBATTLE3.ogg")
      @scene.pbDisplay("Mavros changed to a more powerful form!")
	end,
	"lowHPOpp" => proc do
      @scene.pbTrainerSpeak(["This cannot be happening.",
                             "I will not lose!",
							 "The Void is mine!"
                           ])
	  EliteBattle.playCommonAnimation(:FORMCHANGE, @scene, 1)
	  @battlers[1].pbRaiseStatStageBasic(:DEFENSE, 2)
	  @battlers[1].pbRaiseStatStageBasic(:SPECIAL_DEFENSE, 2)
	  @battlers[1].pbRecoverHP(140)
	  pbBGMPlay("BGM_FINALBATTLE4.ogg")
      @scene.pbDisplay("Mavros changed to a more powerful form!")
	end
  }
  #-----------------------------------------------------------------------------
  # example Dialga fight
  KAIRU = {
    "turnStart0" => proc do
      # hide databoxes
      @scene.pbHideAllDataboxes
      # show flavor text
      @scene.pbDisplay("The battlefield heats up as the resurrected Kairu, the One Remaining, approaches...")
      # change the battle environment (use animation to transition)
      @sprites["battlebg"].reconfigure(:FIERY, :DISTORTION)
      @scene.pbDisplay("Kairu acknowledges your loyalty, and allows you to battle it!")
      # show databoxes
      @scene.pbShowAllDataboxes
    end
  }
  #-----------------------------------------------------------------------------
  # first gym leader
  GYM1 = {
    "afterLastOpp" => proc do
      # hide databoxes
      @scene.pbHideAllDataboxes
      # show flavor text
      @scene.pbTrainerSpeak("I'm in rather a tight spot, aren't I? Well, I refuse to let go!")
	  pbBGMPlay("BGM_BATTLESUPERIOR.ogg")
      @scene.pbShowAllDataboxes
    end
  }
  # fourth gym leader
  GYM4 = {
    "afterLastOpp" => proc do
      # hide databoxes
      @scene.pbHideAllDataboxes
      # show flavor text
      @scene.pbTrainerSpeak("You cannot expect victory. This battle is not yet complete!")
	  pbBGMPlay("BGM_BATTLESUPERIOR.ogg")
      @scene.pbShowAllDataboxes
    end
  }
  # first battle with Donari
  DONARI = {
    "turnStart0" => proc do
	  pname = @battlers[1].name
      tname = @battle.opponent[0].name
      # hide databoxes
      @scene.pbHideAllDataboxes
      # show flavor text
      @scene.pbTrainerSpeak(["I beg of you, stop now!",
                             "Your actions are futile!",
							 "You can't stop what's about to happen."
                           ])
      # show databoxes
      @scene.pbShowAllDataboxes
	end,
	"lowHPOpp" => proc do
      @scene.pbTrainerSpeak(["If you believe you are so strong...",
                             "I will entertain this illusion."
                           ])
	end
  }
end
  #-----------------------------------------------------------------------------
  # example Dialga fight
  ARCEUS = {
    "turnStart0" => proc do
      # hide databoxes
      @scene.pbHideAllDataboxes
      # show flavor text
      @scene.pbDisplay("A strange, astral light illuminates the area as Arceus, the One Above All, approaches...")
      # change the battle environment (use animation to transition)
      @sprites["battlebg"].reconfigure(:GODLY, :DISTORTION)
      @scene.pbDisplay("Arceus takes you to a strange realm, and allows you to battle it!")
      # show databoxes
      @scene.pbShowAllDataboxes
    end,
	"halfHPOpp" => proc do
      @scene.pbDisplay("An ethereal light radiates from Arceus.")
	  EliteBattle.playCommonAnimation(:FORMCHANGE, @scene, 1)
	  @battlers[1].pbRaiseStatStageBasic(:DEFENSE, 2)
	  @battlers[1].pbRaiseStatStageBasic(:SPECIAL_DEFENSE, 2)
	  @scene.pbDisplay("The great Pokémon's power increases!")
	  pbBGMPlay("BGM_VS_OAA2.ogg")
	end
  }